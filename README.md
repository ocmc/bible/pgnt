# Patriarchal Greek New Testament

This repository contains scans of 
<div align="center">

Η 

ΚΑΙΝΗ ΔΙΑΘΗΚΗ 

ΕΓΚΡΙΣΕΙ 

ΤΗΣ ΜΕΓΑΛΗΣ ΤΟΥ ΧΡΙΣΤΟΥ ΕΚΚΛΗΣΙΑΣ 

ΕΝ ΚΩΝΣΤΑΝΤΙΝΟΥΠΟΛΕΙ 

ΕΚ ΤΟΥ ΠΑΤΡΙΑΡΧΙΚΟΥ ΤΥΠΟΓΡΑΦΕΙΟΥ 

1912

The 

New Testament 

(as) Approved (by) 

the Great Church of Christ 

(Published) in Constantinople 

by the Patriarchal Publishing House.

1912
</div>

## Provenance

The scans are from an original copy obtained from [Ἀθηνᾶ Παλαιοβιβλιοπλεῖο](https://palaio-biblio.gr) 25 September, 2023, by Michael A. Colburn.  Scans were made by his wife Melisa A. Colburn.

The name of the original owner is not known. In dark ink on the front cover, there appears to be a name written in cursive.  It is unclear whether this was on the original cover as printed, or added by someone.  The language of the cursive writing is also uncertain.

## Extraneous Items Found

Two items were found enclosed in the book.

1. _A loose, smaller sized page from another Greek New Testament._  Files `90_unknown_NT_1john_pg395.pdf` and `90_unknown_NT_1john_pg396.pdf` are the scans of two sides of a smaller size page that belonged to an unknown Greek New Testament. The page was found inserted loose between two pages of the Patriarchal Greek New Testament. No analysis was made to determine the Greek edition from which the page came.  

2. _A pressed flower._ `91_pressed_flower_found_pg568.pdf` is a scan of a pressed flower that was found on page 568 of the 1912 Patriarchal Greek New Testament. 
